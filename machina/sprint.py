from machina.util import easy_t


def sprint(*args):
    print("[{t}]".format(t=easy_t.time()), *args)