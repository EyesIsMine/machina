from time import strftime
def time(): return strftime("%Y-%m-%d %H:%M:%S")
def date(): return strftime("%Y-%m-%d")