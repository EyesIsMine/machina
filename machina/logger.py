from machina.util import easy_t


class Logger(object):
    def __init__(self, logfile="BROKEN_LOG.log"):
        self.logfile = ''.join(logfile.split(" "))

        self.lf = open(self.logfile, 'w')

    def log(self, data="INVALID DATA PROVIDED", level="DEBUG"):
        self.lf.write("[{}] ".format(easy_t.time()) + str(data) + "\n")
        self.lf.flush()

    def close(self):
        self.lf.close()