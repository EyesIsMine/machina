import platform

from machina.util import easy_t
from .sprint import sprint
from . import logger


class MaM(object):
    def __init__(self):
        self.logger = None
        self.platform = [platform.python_version_tuple(), platform.system(), platform.uname()]
        self.has_scissor_blade = False


    def run(self):
        print("Initializing logger...")
        self.logger = logger.Logger(easy_t.date() + '.log')
        self.logger.log(data="Logger started.")

        try:
            import readline
        except ImportError:
            try:
                import pyreadline as readline
            except ImportError:
                print("Error: No readline support enabled!")
                self.logger.log(level="ERROR", data="No readline module detected! Install pyreadline!")

        sprint("Getting my bearings...")
        sprint("I appear to be running on Python {t[0]}.{t[1]}.{t[2]}".format(t=self.platform[0]))
        sprint("I also appear to be on system {t[1]}".format(t=self.platform))
        sprint("And finally, my uname is {t[2]}, which is confusing.".format(t=self.platform))

        self.logger.log(data=self.platform)

        if self.has_scissor_blade:
            sprint("I am also Ryuko Matoi. Weird.")


        sprint("Dropping to MaM mode.")

    @staticmethod
    def help():
        print("Machina is a text game based around being a mecha and blowing up stuff.")
        print("This is a heavy work in progress, new features may be added and removed at any time.")

    def MaMConsole(self):
        self.logger.log("CONSOLE DROPOFF=1")

        # Run main loop
        nb = 1
        while nb:
            try:
                str_in = str(input("MaM>"))
            except 



